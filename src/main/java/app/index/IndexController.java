package app.index;

import io.javalin.Handler;

public class IndexController {

    public static Handler serveIndexPage = (ctx) -> {
        ctx.redirect("/beauty.html");
    };
}

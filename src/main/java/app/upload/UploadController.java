package app.upload;

import app.Application;
import app.util.ImageDTO;
import io.javalin.Handler;
import io.javalin.UploadedFile;
import io.javalin.json.JavalinJson;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;

import static app.Application.dao;

public class UploadController {

    // TODO verificare utilizzo di PNG per la forma (per il canale alpha)
    public static Handler handleImages = (ctx) -> {
        String wLocation = ctx.formParam("w_location");
        String wTexture = ctx.formParam("w_texture");
        String wColor = ctx.formParam("w_color");
        String wShape = ctx.formParam("w_shape");

        System.out.println(String.format("Weights location:%s, texture:%s, color:%s, shape:%s", wLocation, wTexture, wColor, wShape));

        UploadedFile file = ctx.uploadedFile("file");
        if (file == null) {
            ctx.res.setStatus(400);
            return;
        }

        if (!file.getContentType().startsWith("image")) {
            ctx.res.setStatus(415);
            return;
        }

        String fileName = RandomStringUtils.randomAlphanumeric(15) + file.getExtension();
        String path = Application.TMP_FOLDER_PATH + fileName;
        Files.copy(file.getContent(), Paths.get(path));

        int id = dao.storeImage(fileName);
        dao.generateSignature(id);

        ResultSet rSet = dao.checkSimilarity(id, wColor, wTexture, wShape, wLocation);
        ArrayList<ImageDTO> res = new ArrayList<>();

        while (rSet.next()) {
            InputStream imageIs = rSet.getBinaryStream("img_data");
            byte[] buf = IOUtils.toByteArray(imageIs);
            String encodedImage = Base64.getEncoder().encodeToString(buf);
            res.add(new ImageDTO(encodedImage, rSet.getString("score"), rSet.getInt("img_height"), rSet.getInt("img_width"), rSet.getInt("img_size")));
        }

        dao.deleteImage(id);
        dao.removeFromFS(fileName);
        ctx.result(JavalinJson.toJson(res));
    };

    //TODO validate input
    public static Handler handleColors = (ctx) -> {
        String color_value = ctx.formParam("color_value");
        int r = Integer.valueOf(color_value.substring(1, 3), 16);
        int g = Integer.valueOf(color_value.substring(3, 5), 16);
        int b = Integer.valueOf(color_value.substring(5, 7), 16);
        System.out.println(String.format("%s = (%s, %s, %s)", color_value, r, g, b));

        // la dimensione cambia leggermente i risultati
        int size = 25;
        BufferedImage bufferedImage = new BufferedImage( size, size, BufferedImage.TYPE_INT_RGB );
        for (int x = 0; x < size; x++){
            for (int y = 0; y < size; y++){
                bufferedImage.setRGB(x, y, new Color(r, g, b).getRGB());
            }
        }

        String fileName = RandomStringUtils.randomAlphanumeric(9) + ".jpg";
        String path = Application.TMP_FOLDER_PATH + fileName;

        try {
            ImageIO.write(bufferedImage, "jpg", new File(path));
        } catch ( IOException e) {
            e.printStackTrace();
        }

        int id = dao.storeImage(fileName);
        dao.generateSignature(id);

        // qualunque valore non cambia niente
        ResultSet rSet = dao.checkSimilarity(id, "1.0", "0.0", "0.0", "0.0");
        ArrayList<ImageDTO> res = new ArrayList<>();

        while (rSet.next()) {
            InputStream imageIs = rSet.getBinaryStream("img_data");
            byte[] buf = IOUtils.toByteArray(imageIs);
            String encodedImage = Base64.getEncoder().encodeToString(buf);
            res.add(new ImageDTO(encodedImage, rSet.getString("score"), rSet.getInt("img_height"), rSet.getInt("img_width"), rSet.getInt("img_size")));
        }

        dao.deleteImage(id);
        dao.removeFromFS(fileName);
        ctx.result(JavalinJson.toJson(res));
    };
}

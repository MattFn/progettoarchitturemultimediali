package app;

import app.index.IndexController;
import app.util.DBUtil;
import io.javalin.Javalin;
import app.upload.UploadController;
import app.util.DAO;
import app.util.Path;

import java.sql.SQLException;

import static io.javalin.apibuilder.ApiBuilder.*;

public class Application {

    public static final String TMP_FOLDER_PATH = "C:\\tmp\\";
    public static final String SID = "orcl";

    public static DAO dao;


    public static void main(String[] args) {

        try {
            dao = new DAO("c##archimulti", "archimulti", 1521, SID);
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

       if (args.length > 0 && args[0].equals("init")) {
           System.out.println("database init");
           try {
               DBUtil.populateDatabase();
           } catch (SQLException e) {
               e.printStackTrace();
           }
       }

        Javalin app = Javalin.create()
                .enableStaticFiles("/public")
                .start(7000);

        app.routes(() -> {
            before((context) -> System.out.println(context.path()));
            get("/", IndexController.serveIndexPage);
            get(Path.Web.INDEX, IndexController.serveIndexPage);
            post(Path.Web.COLOR, UploadController.handleColors);
            post(Path.Web.QUERY_IMAGES, UploadController.handleImages);
        });
    }
}

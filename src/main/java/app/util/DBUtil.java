package app.util;

import app.Application;

import java.io.File;
import java.sql.SQLException;

public class DBUtil {

    private static  DAO dao;

    public static void populateDatabase() throws SQLException {
        dao = new DAO("c##archimulti", "archimulti", 1521, Application.SID);

        File[] files = new File(Application.TMP_FOLDER_PATH).listFiles();

        //TODO load only images
        for (File file : files) {
            System.out.println("File: " + file.getName());
            dao.storeImage(file.getName());
        }

        dao.generateAllSignatures();
    }
}

package app.util;

import java.io.File;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import app.Application;
import oracle.jdbc.pool.OracleDataSource;

public class DAO {

    //TODO convert to float?
    static final String TEXTURE_W = "0.0";
    static final String COLOR_W = "0.7";
    static final String LOCATION_W = "1.0";
    static final String SHAPE_W = "0.3";

    static final int LIMIT = 4;

    private OracleDataSource dataSource;


    public DAO(String user, String password, int port, String SID) throws SQLException {
        dataSource = new OracleDataSource();
        dataSource.setURL(String.format("jdbc:oracle:thin:@localhost:%d:%s", port, SID));
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setPortNumber(port);
    }

    public boolean ensureCreated() throws SQLException {
        Connection conn = dataSource.getConnection();
        Statement stmt = conn.createStatement();
        ArrayList<String> tables = new ArrayList();

        ResultSet res = stmt.executeQuery(
                "SELECT table_name " +
                "FROM user_tables " +
                "WHERE table_name IN ('TEST', 'IMAGE')");

        while (res.next()) {
            tables.add(res.getString("TABLE_NAME"));
        }

        return tables.size() == 2;
    }

    public void deleteImage(int id) throws SQLException {
        Connection conn = dataSource.getConnection();
        Statement stmt = conn.createStatement();

        ResultSet res = stmt.executeQuery("DELETE FROM image i WHERE i.image_id = " + id);
    }

    public void removeFromFS(String fileName) {
        File file = new File(Application.TMP_FOLDER_PATH + fileName);

        if(file.delete()) {
            System.out.println("File " + fileName + " removed");
        }
        else {
            System.out.println("Failed to delete the file " + fileName);
        }
    }

    public void generateAllSignatures() throws SQLException {
        Connection conn = dataSource.getConnection();

        String jobquery =
                "DECLARE\n" +
                "    ctx RAW(4000):= NULL;\n" +
                "BEGIN\n" +
                "    FOR img in (SELECT * FROM IMAGE I FOR UPDATE)\n" +
                "    LOOP\n" +
                "        img.image.import(ctx);\n" +
                "        img.image_sig.generateSignature(img.image);\n" +
                "        \n" +
                "        UPDATE image im\n" +
                "        SET im.image = img.image,\n" +
                "            im.image_sig = img.image_sig\n" +
                "        WHERE im.image_id = img.image_id;\n" +
                "    END LOOP;\n" +
                "    COMMIT;\n" +
                "END;";

        CallableStatement callStmt = conn.prepareCall(jobquery);
        callStmt.execute();
    }

    public void generateSignature(int id) throws SQLException {
        Connection conn = dataSource.getConnection();

        String jobquery =
                "DECLARE\n" +
                    "myimg ORDSYS.ORDImage;\n" +
                    "mysig ORDSYS.ORDImageSignature;\n" +
                    "ctx RAW(4000):= NULL;\n" +
                "BEGIN\n" +
                    "SELECT S.image, S.image_sig INTO myimg, mysig\n" +
                    "FROM image S\n" +
                    "WHERE S.image_id = ? FOR UPDATE;\n" +
                    "myimg.import(ctx);\n" +
                    "mysig.generateSignature(myimg);\n" +
                    "UPDATE image S\n" +
                    "SET S.image = myimg,\n" +
                    "S.image_sig = mysig\n" +
                    "WHERE S.image_id = ?;\n" +
                    "COMMIT;\n" +
                "END;";

        CallableStatement callStmt = conn.prepareCall(jobquery);
        callStmt.setInt(1, id);
        callStmt.setInt(2, id);

        callStmt.execute();
    }

    public int storeImage(String name) throws SQLException {
        int id = getCurrentId();

        Connection conn = dataSource.getConnection();
        String jobquery =
                "DECLARE\n" +
                    "img ORDSYS.ORDImage;\n" +
                    "ctx RAW(4000) := NULL;\n" +
                "BEGIN\n" +
                    "INSERT INTO image \n" +
                    "VALUES (?, ORDSYS.ORDImage.init(), ORDSYS.ORDImageSignature.init());\n" +
                    "SELECT i.image INTO img FROM image i WHERE image_id = ? for UPDATE;\n" +
                    "img.setSource('file','TMP',?);\n" +
                    "img.import(ctx);\n" +
                    "UPDATE image SET image = img WHERE image_id = ?; \n" +
                    "COMMIT;\n" +
                "END;";

        CallableStatement callStmt = conn.prepareCall(jobquery);
        callStmt.setInt(1, id);
        callStmt.setInt(2, id);
        callStmt.setString(3, name);
        callStmt.setInt(4, id);

        callStmt.execute();

        return id;
    }

    //TODO check inputs range
    //TODO controllare che i query params siano quelli attesi (funziona con undefined???)
    public ResultSet checkSimilarity(int id, String colorW, String textureW, String shapeW, String locationW) throws SQLException {
        String query =
            "SELECT p.image.source.localData as img_data, p.image as img, p.image_id as img_id, p.image.getheight() as img_height,\n" +
                    "p.image.getwidth() as img_width, p.image.getcontentlength() as img_size, " +
                    "ORDSYS.ORDImageSignature.evaluateScore(p.image_sig, c.image_sig, ?) as score\n" +
            "FROM image p, image c\n" +
            "WHERE c.image_id = ? AND c.image_id != p.image_id\n" +
            "ORDER BY score \n" +
            "FETCH FIRST ? ROWS ONLY";

        Connection conn = dataSource.getConnection();
        PreparedStatement stmt = conn.prepareStatement(query);
        stmt.setString(1, String.format("color=\"%s\",texture=\"%s\",shape=\"%s\",location=\"%s\"",  colorW, textureW, shapeW, locationW));
        stmt.setInt(2, id);
        stmt.setInt(3, LIMIT);

        return stmt.executeQuery();
    }

    public ResultSet checkSimilarity(int id) throws SQLException {
        return checkSimilarity(id, COLOR_W, TEXTURE_W, SHAPE_W, LOCATION_W);
    }

    public int getCurrentId() throws SQLException {
        String query =  "SELECT NVL(MAX(i.image_id), 0) AS MAX_VAL " +
                        "FROM IMAGE i";

        Connection conn = dataSource.getConnection();
        Statement stmt = conn.createStatement();

        ResultSet rset = stmt.executeQuery(query);

        if (rset.next())
            return rset.getInt("MAX_VAL") + 1;

        return 1;
    }

    public static void main(String[] args) throws SQLException {
        DAO dao = new DAO("c##archimulti", "archimulti", 1521, Application.SID);
        dao.generateAllSignatures();

    }
}

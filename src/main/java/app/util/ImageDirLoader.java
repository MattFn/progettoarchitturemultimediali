package app.util;

import java.io.File;
import java.sql.SQLException;

public class ImageDirLoader {

    public static final String FOLDER_PATH = "C:\\tmp";

    public static void main(String[] args) throws SQLException {

        DAO dao = null;

        try {
            dao = new DAO("c##archimulti", "archimulti", 1521, "ORCL");
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        File folder = new File(FOLDER_PATH);
        File[] files = folder.listFiles();

        for (File f : files) {
            dao.storeImage(f.getName());
            System.out.println("file " + f.getName() + " inserted");
        }
        
    }
}

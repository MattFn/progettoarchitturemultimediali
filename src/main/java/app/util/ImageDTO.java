package app.util;

public class ImageDTO {

    String encodedImage;
    String similarity;
    int height;
    int width;
    int size;

    public ImageDTO(String encodedImage, String similarity, int height, int width, int size) {
        this.encodedImage = encodedImage;
        this.similarity = similarity;
        this.height = height;
        this.width = width;
        this.size = size;
    }

    public String getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }

    public String getSimilarity() {
        return similarity;
    }

    public void setSimilarity(String similarity) {
        this.similarity = similarity;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}

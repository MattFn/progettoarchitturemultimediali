package app.util;

public class Path {

    public static class Web {

        public static final String INDEX = "/index";
        public static final String UPLOAD = "/upload";
        public static final String COLOR = "/colors";
        public static final String QUERY_IMAGES = "/images";
    }

}
